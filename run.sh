#!/bin/bash

sysctl -w net.ipv4.conf.all.rp_filter=2
systcl -w net.ipv4.ip_forward=1
sysctl net.ipv6.conf.all.forwarding=1

iptables --table nat --append POSTROUTING --jump MASQUERADE
echo 1 > /proc/sys/net/ipv4/ip_forward
for each in /proc/sys/net/ipv4/conf/*
do
	echo 0 > $each/accept_redirects
	echo 0 > $each/send_redirects
done


cat > /etc/ppp/l2tp-secrets <<EOF
# This file holds secrets for L2TP authentication.
# Username  Server  Secret  Hosts

EOF

cat > /etc/ipsec.secrets <<EOF
# This file holds shared secrets or RSA private keys for authentication.
# RSA private key for this host, authenticating it to any other host
# which knows the public part.  Suitable public keys, for ipsec.conf, DNS,
# or configuration of other implementations, can be extracted conveniently
# with "ipsec showhostkey".
178.62.246.16 188.166.57.61 : PSK "testKey"

EOF

if [ -f "/etc/ipsec.d/l2tp-secrets" ]; then
	echo "Overwriting standard /etc/ppp/l2tp-secrets with /etc/ipsec.d/l2tp-secrets"
	cp -f /etc/ipsec.d/l2tp-secrets /etc/ppp/l2tp-secrets
fi

if [ -f "/etc/ipsec.d/ipsec.secrets" ]; then
	echo "Overwriting standard /etc/ipsec.secrets with /etc/ipsec.d/ipsec.secrets"
	cp -f /etc/ipsec.d/ipsec.secrets /etc/ipsec.secrets
fi

if [ -f "/etc/ipsec.d/ipsec.conf" ]; then
	echo "Overwriting standard /etc/ipsec.conf with /etc/ipsec.d/ipsec.conf"
	cp -f /etc/ipsec.d/ipsec.conf /etc/ipsec.conf
fi

if [ -f "/etc/ipsec.d/strongswan.conf" ]; then
	echo "Overwriting standard /etc/strongswan.conf with /etc/ipsec.d/strongswan.conf"
	cp -f /etc/ipsec.d/strongswan.conf /etc/strongswan.conf
fi

if [ -f "/etc/ipsec.d/xl2tpd.conf" ]; then
	echo "Overwriting standard /etc/xl2tpd/xl2tpd.conf with /etc/ipsec.d/xl2tpd.conf"
	cp -f /etc/ipsec.d/xl2tpd.conf /etc/xl2tpd/xl2tpd.conf
fi

mkdir -p /var/run/xl2tpd

exec /usr/bin/supervisord -c /supervisord.conf
